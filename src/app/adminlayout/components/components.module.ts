import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DxVectorMapModule } from 'devextreme-angular';



@NgModule({
  imports: [CommonModule, RouterModule, NgbModule, DxVectorMapModule],
  declarations: [

  ],
  exports: [

  ]
})
export class ComponentsModule {}
