import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminlayoutRoutingModule } from './adminlayout-routing.module';
import { AdminlayoutComponent } from './adminlayout.component';


import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { NgbdModalBasic } from './components/modal/modal.component';
import { ImageUploadComponent } from './components/image-upload/image-upload.component';
import { FileInputComponent } from './components/file-input/file-input.component';
import { PictureUploadComponent } from './components/picture-upload/picture-upload.component';

@NgModule({
  imports: [
    CommonModule,
    AdminlayoutRoutingModule,
    NgbModule
  ],
  declarations: [AdminlayoutComponent, 
    AdminLayoutComponent, AuthLayoutComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    NgbdModalBasic,
    ImageUploadComponent,
    FileInputComponent,
    PictureUploadComponent


  ],
  exports: [AdminlayoutComponent, 

    AdminLayoutComponent, AuthLayoutComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    NgbdModalBasic,
    ImageUploadComponent,
    FileInputComponent,
    PictureUploadComponent
  ]
})
export class AdminlayoutModule { }
