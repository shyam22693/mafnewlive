import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      { path: '', loadChildren: './layout/layout.module#LayoutModule'},
      { path: 'admin', loadChildren: './adminlayout/adminlayout.module#AdminlayoutModule'},
      { path: 'agent', loadChildren: './agentlayout/agentlayout.module#AgentlayoutModule'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
