import { TestBed, async, inject } from '@angular/core/testing';

import { AuthagentGuard } from './authagent.guard';

describe('AuthagentGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthagentGuard]
    });
  });

  it('should ...', inject([AuthagentGuard], (guard: AuthagentGuard) => {
    expect(guard).toBeTruthy();
  }));
});
