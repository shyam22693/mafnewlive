import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { HomeComponent } from './home/home.component';

import { RegisterComponent } from './register/register.component';
import { PaymentComponent } from './payment/payment.component';
import { ThankuComponent } from './thanku/thanku.component';
import { AboutComponent } from './about/about.component';
import { BecomeagentComponent } from './becomeagent/becomeagent.component';
import { ContactusComponent } from './contactus/contactus.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';

import { AdminsignupComponent } from './adminsignup/adminsignup.component';


import { PaymentfailComponent } from './paymentfail/paymentfail.component';


import { AuthGuard } from './../auth.guard';
import { AuthagentGuard } from './../authagent.guard';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'prefix' },
      { path: 'home', component: HomeComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'payment', component: PaymentComponent },
      { path: 'thankyou', component: ThankuComponent },
      { path: 'aboutus', component: AboutComponent },
      { path: 'agent', component: BecomeagentComponent },
      { path: 'contactus', component: ContactusComponent },
      { path: 'login', component: LoginComponent },
      { path: 'admin', component: AdminComponent },
  

   //   { path: 'admin-signup', component: AdminsignupComponent },
  

      { path: 'paymentfail', component: PaymentfailComponent },
      // AdminDashboard




    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
