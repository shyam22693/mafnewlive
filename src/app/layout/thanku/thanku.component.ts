import { Component, OnInit } from '@angular/core';
import {AuthorizationService} from "../../customservices/authorization.service";
import {NgForm} from "@angular/forms";
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-thanku',
  templateUrl: './thanku.component.html',
  styleUrls: ['./thanku.component.css']
})
export class ThankuComponent implements OnInit {

  emailVerificationMessage: boolean = false;
  tempAgentData :any;
  firstName :string;
  emailId :string;

  constructor(private auth: AuthorizationService,
              private _router: Router,private cookieService: CookieService) {}

  ngOnInit() {
    this.tempAgentData = JSON.parse(this.cookieService.get('AHJSKIDGTERCDHDFCSKDHDGDT'));
    this.firstName = this.tempAgentData.firstName;
    this.emailId = this.tempAgentData.emailId;
	}

}

