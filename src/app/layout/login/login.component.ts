import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators, FormArray, FormControl, NgForm } from '@angular/forms';
import { MustMatch } from '../../customservices/passwordmatch.validator';
import { AuthorizationService } from '../../customservices/authorization.service';

import { CookieService } from 'ngx-cookie-service';

import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';

const poolData = {
  UserPoolId: 'us-east-1_B5waepwSZ', // Your user pool id here
  ClientId: '4gkgpcug955h6kfapt4n61ojkr' // Your client id here  
};

const userPool = new CognitoUserPool(poolData);



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  confirmCode: boolean = false;
  codeWasConfirmed: boolean = false;
  error: string = '';
  agentId: string;
  submitted = false;
  user: any;
  agentloginform: FormGroup;
  bAuthenticated = false;
  constructor(private formBuilder: FormBuilder, private agentAuth: AuthorizationService, public router: Router, private activatedRoute: ActivatedRoute,private cookieService: CookieService) { }
  ngOnInit() {
    this.agentloginform = this.formBuilder.group({
      agentId: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });


    var authenticatedUser = this.agentAuth.getAuthenticatedUser();
    console.log('authenticatedUser', authenticatedUser)
    if (authenticatedUser == null) {
      return;
    }
    this.bAuthenticated = true;
  }

  get ad() { return this.agentloginform.controls; }
  onLogin(user) {
    this.user = {
      agentid: this.agentloginform.get('agentId').value,
      password: this.agentloginform.get('password').value
    };

    console.log('cccc', this.user);
    this.submitted = true;
    console.log('Register', this.user);
    if (this.agentloginform.invalid) {
      console.log('please fill all fields');
      return;
    }

    let agentid = this.user.agentid;
    let password = this.user.password;

    console.log('cccccccccccccccc', agentid, password);

    this.agentAuth.signIn(agentid, password).subscribe(
      (data) => {
        this.confirmCode = false;
        let cognitoUser = userPool.getCurrentUser();
        if (cognitoUser != null) {
          let cognitoUser = userPool.getCurrentUser();
          let agentid = 'dummy';
          cognitoUser.getSession((err, session) => {
            cognitoUser.getUserAttributes((err, result) => {
              console.log('*************', result[0].getValue())
              // this.agentId = result[0].getValue();
              // var agentId = result[0].getValue();
              console.log('ccccc', this.router)
              let agentId = {agentId:result[0].getValue()};
              this.cookieService.set('AXRGBSYEQWATNLKFSYT', JSON.stringify(agentId));
              this.router.navigate(['/agent-profile', result[0].getValue()])
            })
          });
        }
      },
      (err) => {
        console.log(err);
        alert('Email ID or Password Mismatch!')
        this.error = 'Registration Error has occurred';
      }
    );
  }
}

