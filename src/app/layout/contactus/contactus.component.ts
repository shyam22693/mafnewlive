import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { AgentService } from '../../../agent.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  contactform: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder,private agent: AgentService) { }

  ngOnInit() {
    this.contactform = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      sub: [null, [Validators.required]],
      message: [null, [Validators.required]],
    }
    );
  }
  get f() { return this.contactform.controls; }
  onContact(user) {
    this.submitted = true;

    console.log('user user',user);
    let data = user;

    if (this.contactform.invalid) {
      console.log('please fill all fields');
      return;
    }
    else{
      console.log('get the data!');
    this.agent.sendEnquiry(data)
      .subscribe(res => {
        alert('Thanks for enquiry.Our team will get back to You!')
      }, (err) => {
        console.log(err);
      });

    }
  }
}



