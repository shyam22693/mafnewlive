import { Injectable } from '@angular/core';
import {AuthenticationDetails, CognitoUser, CognitoUserPool,CognitoUserAttribute} from 'amazon-cognito-identity-js';
import { CookieService } from 'ngx-cookie-service';
import { Observable, of } from 'rxjs';

const poolData = {
  UserPoolId: 'us-east-1_B5waepwSZ', // Your user pool id here
  ClientId: '4gkgpcug955h6kfapt4n61ojkr' // Your client id here  
};

const userPool = new CognitoUserPool(poolData);

@Injectable()
export class AuthorizationService {
  cognitoUser: any;

  constructor(private cookieService: CookieService) { }

  register(email, password, agentid) {
    var attributeList = [];

    let dataAgentId = {
        Name : 'custom:agentId',
        Value : agentid
    };
    let attributeAgentId = new CognitoUserAttribute(dataAgentId)

    attributeList.push(attributeAgentId);
    return Observable.create(observer => {
      userPool.signUp(agentid, password, attributeList, null, (err, result) => {
        if (err) {
          console.log("signUp error", err);
          observer.error(err);
        }
        else{
          this.cognitoUser = result.user;
          console.log("signUp success", result);
          observer.next(result);
          observer.complete();
        }
      });
    });
  }




  // confirmAuthCode(code) {
  //   const user = {
  //     Username : this.cognitoUser.username,
  //     Pool : userPool
  //   };
  //   return Observable.create(observer => {
  //     const cognitoUser = new CognitoUser(user);
  //     cognitoUser.confirmRegistration(code, true, function(err, result) {
  //       if (err) {
  //         console.log(err);
  //         observer.error(err);
  //       }
  //       console.log("confirmAuthCode() success", result);
  //       observer.next(result);
  //       observer.complete();
  //     });
  //   });
  // }

  signIn(agentid, password) { 

    const authenticationData = {
      Username : agentid,
      Password : password,
    };
    const authenticationDetails = new AuthenticationDetails(authenticationData);

    const userData = {
      Username : agentid,
      Pool : userPool
    };
    const cognitoUser = new CognitoUser(userData);
    
    return Observable.create(observer => {

      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (result) {
          
          //console.log(result);
          observer.next(result);
          observer.complete();
        },
        onFailure: function(err) {
          console.log(err);
          observer.error(err);
        },
      });
    });
  }

  isLoggedIn() {    
    return userPool.getCurrentUser() != null;
  }

  getAuthenticatedUser() {
    return userPool.getCurrentUser();
  }

  getFalseOnIntruder()
  {

    let agentId = localStorage.getItem('CognitoIdentityServiceProvider.4gkgpcug955h6kfapt4n61ojkr.LastAuthUser');
    let gatData = this.cookieService.get('AXRGBSYEQWATNLKFSYT')? JSON.parse(this.cookieService.get('AXRGBSYEQWATNLKFSYT')):null;
    let aid =gatData!=null? gatData.agentId : null;

     if(agentId!=null && aid!=null && agentId==aid)
     {
       return true;
     }
     else{
       return false;
     }
  }

  logOut() {
    this.getAuthenticatedUser().signOut();
    this.cookieService.delete('AXRGBSYEQWATNLKFSYT');
    this.cognitoUser = null;
  }
}
