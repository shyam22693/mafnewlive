import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgentlayoutComponent } from './agentlayout.component';


import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'components',
        loadChildren:
          './pages/components/components.module#ComponentsPageModule'
      },
      {
        path: 'forms',
        loadChildren: './pages/forms/forms.module#Forms'
      },
      {
        path: 'tables',
        loadChildren: './pages/tables/tables.module#TablesModule'
      },
      {
        path: 'widgets',
        loadChildren: './pages/widgets/widgets.module#WidgetsModule'
      },
      {
        path: 'charts',
        loadChildren: './pages/charts/charts.module#ChartsModule'
      },
      {
        path: '',
        loadChildren:
          './pages/pages/user-profile/user-profile.module#UserModule'
      },
      {
        path: '',
        loadChildren: './pages/pages/timeline/timeline.module#TimelineModule'
      }
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'pages',
        loadChildren: './pages/pages/pages.module#PagesModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentlayoutRoutingModule { }
